#include "NativePad.h"

using namespace std;
using namespace glm;

#define clock std::chrono::steady_clock
#define timer std::chrono::time_point<clock>
#define timediff std::chrono::duration<double>

timer start;
cbuffer<glm::vec3> accs(100);
cbuffer<glm::vec3> gyrs(10);
cbuffer<glm::vec3> mags(100);
cbuffer<glm::vec3> gyrobuffer(100);
glm::vec3 gyrobias;
glm::quat q;
glm::quat qgyr;
glm::quat qmag;

JNIEXPORT void JNICALL Java_it_omidev_ue4droidpad_SensorFusion_Init
  (JNIEnv *, jclass)
{
	start = clock::now();
	accs.m_count = 0;
	gyrs.m_count = 0;
	mags.m_count = 0;
	qgyr = glm::quat();
	qmag = glm::quat();
	q = glm::quat();
}

JNIEXPORT void JNICALL Java_it_omidev_ue4droidpad_SensorFusion_UpdateMagnetometer
  (JNIEnv *, jclass, jfloat x, jfloat y, jfloat z)
{
	glm::vec3 mag(x, y, z);
	mags.add(mag);
}

JNIEXPORT void JNICALL Java_it_omidev_ue4droidpad_SensorFusion_UpdateAccelerometer
  (JNIEnv *, jclass, jfloat x, jfloat y, jfloat z)
{
	glm::vec3 acc(x, y, z);
	accs.add(acc);
}

JNIEXPORT void JNICALL Java_it_omidev_ue4droidpad_SensorFusion_UpdateGyroscope
  (JNIEnv *, jclass, jfloat x, jfloat y, jfloat z)
{
	timer stop = clock::now();
	timediff diff = stop - start;
	start = stop;

	float dt = static_cast<float>(diff.count());
	if (dt < 0.00001)
		return;

	glm::vec3 gyr(x, y, z);
	if (gyrobuffer.m_count < gyrobuffer.m_capacity)
	{
		gyrobuffer.add(gyr);
		if (gyrobuffer.m_count == gyrobuffer.m_capacity)
		{
			gyrobias = gyrobuffer.average();
			LOG("gyro bias %f %f %f", gyrobias.x, gyrobias.y, gyrobias.z);
		}
	}
	else
	{
		gyr = gyr - gyrobias;
//		if (fabs(gyr.x) < 0.01f) gyr.x = 0;
//		if (fabs(gyr.y) < 0.01f) gyr.y = 0;
//		if (fabs(gyr.z) < 0.01f) gyr.z = 0;
		gyrs.add(gyr);
		//gyr = gyrs.average();

		float velLen = glm::length(gyr);
		if (velLen > 0)
		{
			// Gyro integration
			qgyr = glm::normalize(qgyr * glm::angleAxis(velLen * dt, gyr / velLen));

  			// Gyro pitch correction with accelerometer
			glm::vec3 wacc = glm::normalize(qgyr * accs.average());
			glm::vec3 tiltAxis = glm::cross(glm::vec3(0,1,0), wacc);
			float crossLen = glm::length(tiltAxis);
			if (fabsf(crossLen) > 0.1)
			{
				float tiltAngle = glm::asin(crossLen);
				qgyr = glm::angleAxis(-tiltAngle * 0.2f, tiltAxis / crossLen) * qgyr;
			}
			qgyr = glm::normalize(qgyr);

			// Mag pitch correction
			{
				glm::vec3 acc = glm::normalize(accs.average());
				glm::vec3 mag = glm::normalize(mags.average());
				float magDist = glm::dot(acc, mag);
				glm::vec3 magProj = glm::normalize(mag - acc * magDist); // Remove vertical component
				glm::vec3 x = glm::cross(acc, magProj);
				glm::mat3 magmat(x.x, x.y, x.z, acc.x, acc.y, acc.z, magProj.x, magProj.y, magProj.z);
				qmag = glm::quat_cast(magmat);
			}

			// Yaw interpolation
			//qgyr = glm::slerp(qgyr, qmag, 0.001f);
		}
	}
}

JNIEXPORT jfloat JNICALL Java_it_omidev_ue4droidpad_SensorFusion_GetCurrentAngle
  (JNIEnv *, jclass)
{
	glm::vec3 m = glm::normalize(qgyr * glm::vec3(1,0,0));
	float angle = atan2(m.z, m.x);
	return glm::degrees(angle);
}

JNIEXPORT jfloatArray JNICALL Java_it_omidev_ue4droidpad_SensorFusion_GetWorldMagnetometer
  (JNIEnv *env, jclass)
{
	jfloatArray result = env->NewFloatArray(3);
	//glm::vec3 m = (q * mags.average() - glm::vec3(-4,0,-4)) * glm::vec3(.9f, 1, 1.f);
	glm::vec3 m = qgyr * glm::vec3(20,0,0);
	jfloat worldMag[] = { m.x, m.y, m.z };
	env->SetFloatArrayRegion(result, 0, 3, worldMag);
	return result;
}
